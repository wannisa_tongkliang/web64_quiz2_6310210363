const mysql = require('mysql');
const TOKEN_SECRET = process.env.TOKEN_SECRET

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'music_admin',
    password : 'music_admin',
    database :'MusicSystem'

});
connection.connect();

const express = require('express')
const app = express()
const port = 4000


/*API for Processing UserMusic Authorization */
app.post("/login" , (req, res)=> {
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM UserMusic WHERE Username='${username}'`
    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                     "status" : "400",
                     "message": "Error querying from running db"
                  })
          }else {
             let db_password = rows[0].Password
              bcrypt.compare(user_password, db_password, (err, result) => {
                 if (result) {
                    let payload = {
                          "username" : rows[0].Username,
                          "user_id" : rows[0].RunnerID,
                          "IsAdmin" : rows[0].IsAdmin,
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d'})
                    res.send(token)
                 }else { res.send("Invalid") }
              })   
        }

    })
})




app.post("/register_music", authenticateToken, (req, res)  =>{
    let user_profile = req.user 
    let music_id = req.query.music_id
    let UserMusic = req.query.UserMusic
    let SchoolID = req.query.SchoolID
   let query = ` INSERT INTO Registration 
                  (MusicID, UserMusic, SchoolID,RegistrationTime)
                  VALUES ('${music_id}',
                          '${UserMusic}', 
                           ${SchoolID}, 
                           NOW() )`
    console.log(query)           
    
    connection.query( query, (err, rows) => {
        if(err){
            res.json({
                       "status" : "400",
                       "message": "Error inserting data into db"
                    })
            }else{
                res.json({
                    "status" : "200",
                    "message": "Adding event succesful"  
             })
          }
     });

});

/* CRUD Operation  */
app.get("/list_music" , (req, res) =>{
    let query = "SELECT * from MusicSchool";
   connection.query( query, (err, rows) => {
     if(err){
         res.json({
                    "status" : "400",
                    "message": "Error querying from running db"
                 })
         }else{
             res.json(rows)
          }
       });
})

app.post("/Add_music" , (req, res) => {

    let music_name = req.query.music_name
    let music_location = req.query.music_location
    let query = ` INSERT INTO MusicSchool 
                   (MusicName, MusicLocation)
                   VALUES ('${music_name}','${music_location}' )`
     console.log(query)           
     connection.query( query, (err, rows) => {
         if(err){
             res.json({
                        "status" : "400",
                        "message": "Error inserting data into db"
                     })
             }else{
                 res.json({
                     "status" : "200",
                     "message": "Adding event succesful"  
              })
           }
      });
 
  })

  

app.post("/update_music" , (req, res) => {

    let music_id = req.query.music_id
    let music_name = req.query.music_name
    let music_location = req.query.music_location
 
    let query = `UPDATE MusicSchool SET
                   MusicName= '${music_name}',
                   MusicLocation='${music_location}'
                   WHERE MusicID=${music_id}`

     console.log(query)           
     
     connection.query( query, (err, rows) => {
         if(err){
             console.log(err)
             res.json({
                        "status" : "400",
                        "message": "Error updating record"
                     })
             }else{
                 res.json({
                     "status" : "200",
                     "message": "Updating event succesful"  
              })
           }
      });
 
  })

  app.post("/delete_music" , (req, res) => {

    let music_id = req.query.music_id

    let query = `DELETE FROM  WHERE MusicID=${music_id}`

     console.log(query)           
     
     connection.query( query, (err, rows) => {
         if(err){
             console.log(err)
             res.json({
                        "status" : "400",
                        "message": "Error deleting record"
                     })
             }else{
                 res.json({
                     "status" : "200",
                     "message": "Deleting record success"  
              })
           }
      });
 
  })

  app.listen(port, () => {
    console.log(` Now starting Music System Backend ${port} `)
})

/*
query = "SELECT * from UserMusic";
connection.query( query, (err, rows) => {
    if(err) {
        console.log(err);
    }else {
        console.log(rows);
    }

});

connection.end();*/